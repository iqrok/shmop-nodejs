# shmop C binding

## Usage
```javascript
const shmop = require('@iqrok/shmop');
```
----
## Methods
### setPath(path)
- **Descriptions**
    set path to generate token with ftok()
- **Parameter(s)**
    - __path__ [*String*]
    path to existing file
----

### getToken(id, path = null)
- **Descriptions**
    generate token with ftok()
- **Parameter(s)**
    - __id__ [*Number*]
    number from 1 - 255. if 0 is passed as id, it will throw error
    - ___path___ (_optional_)
    use path defined via this parameter instead of global path defined via __setPath()__
- **Return**
    [*Number*] generated token
----
    
### read(id, type)
- **Descriptions**
    read shared memory block pointed by the provided token
- **Parameter(s)**
    - __id__ [*Number*]
    if __id__ <= 255, then it will be passed to __getToken()__, otherwise it will be treated as the token
    - __type__ [*String*]
    data type that will be read 
- **Return**
    [*Number*] Read shared memory
----

### write(id, value, type)
- **Descriptions**
    read shared memory block pointed by the provided token
- **Parameter(s)**
    - __id__ [*Number*]
    if __id__ <= 255, then it will be passed to __getToken()__, otherwise it will be treated as the token
    - __value__ [*Number*]
    value that will be written to shared memory block 
    - __type__ [*String*]
    data type that will be read 
- **Return**
    [*Number*] Read shared memory
----
## Valid Data Types
|Type|Description|
|---|---|
|**uint8**|8-bit unsigned integer|
|**int8**|8-bit signed integer|
|**uint16**|16-bit unsigned integer|
|**int16**|16-bit signed integer|
|**uint32**|32-bit unsigned integer|
|**int32**|32-bit signed integer|
|**float**|32-bit single precision floating number|
|**double**|64-bit double precision floating number|