const SHMOP = require('./build/Release/shmop.node');

class __shmop{
	constructor(){
		this._path = undefined;
	};

	setPath(_path){
		if(_path){
			this._path = _path;
			return true;
		}

		return false;
	};

	getToken(id, _path = null){
		if(+id === 0){
			throw 'id must be larger than 0';
		}

		return SHMOP.ftok(_path || this._path, id);
	};

	read(id, type){
		const token = id <= 0xff ? this.getToken(id) : id;
		return SHMOP.shmat(token, type);
	};

	write(id, value, type){
		const token = id <= 0xff ? this.getToken(id) : id;
		return SHMOP.shmat(token, type, value);
	};
}

module.exports = new __shmop();
