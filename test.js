const shmop = require('./');

const filepath = '/tmp/test';

shmop.setPath(filepath);

const __loop = () => {
	const read1 = shmop.read(0x1f000000, 'int16');
	const read2 = shmop.read(0x1f000001, 'int16');
	console.log(read1.toString(16), read2.toString(16));

	setTimeout(__loop, 50)
};

__loop();
//~ const write = shmop.write(0x1f000000, (Date.now() % 1000) * 0.015343 * 2.54221, 'float');
//~ console.log(write);

//~ const read2 = shmop.read(0x1f000000, 'int16');
//~ console.log(read2);
