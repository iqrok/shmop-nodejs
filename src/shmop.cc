#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>

#include <napi.h>

using namespace std;
using namespace Napi;

Value SHMOP_ftok(const CallbackInfo& info) {
	Env env = info.Env();

	if (info.Length() < 2 || !info[0].IsString() || !info[1].IsNumber()){
		TypeError::New(env, "Expected 2 Parameter(s) to be passed [ Number, String ]")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	uint32_t _id = info[1].As<Number>();

	if (_id == 0){
		TypeError::New(env, "Second Parameter's value can't be 0")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	const char* _path = info[0].ToString().Utf8Value().c_str();

	int file;
	if ( (file = open(_path, O_RDWR, 0x777)) >= 0 ) {
		close(file);
	} else {
		file = open(_path, O_RDWR | O_CREAT, 0x777);
		close(file);
	}

	key_t key = ftok((char*)_path, _id);

	return Number::New(env, key);
}

Value SHMOP_shmat(const CallbackInfo& info) {
	Env env = info.Env();

	if (info.Length() < 2 || !info[0].IsNumber() || !info[1].IsString()){
		TypeError::New(env, "Expected at least 2 Parameter(s) to be passed [ Number, String [,Number] ]")
			.ThrowAsJavaScriptException();

		return Number::New(env, Number::New(env, 0));
	}

	key_t key = info[0].As<Number>();
	string datatype = info[1].As<String>().Utf8Value();

	Number retVal;

	if(datatype == "int8"){
		int shmid = shmget(key, sizeof(int8_t), 0666|IPC_CREAT);
		int8_t *value = (int8_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			int32_t _value = info[2].As<Number>();
			*value = (int8_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "uint8"){
		int shmid = shmget(key, sizeof(uint8_t), 0666|IPC_CREAT);
		uint8_t *value = (uint8_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			uint32_t _value = info[2].As<Number>();
			*value = (uint8_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "int16"){
		int shmid = shmget(key, sizeof(int16_t), 0666|IPC_CREAT);
		int16_t *value = (int16_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			int32_t _value = info[2].As<Number>();
			*value = (int16_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "uint16"){
		int shmid = shmget(key, sizeof(uint16_t), 0666|IPC_CREAT);
		uint16_t *value = (uint16_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			uint32_t _value = info[2].As<Number>();
			*value = (uint16_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "int32"){
		int shmid = shmget(key, sizeof(int32_t), 0666|IPC_CREAT);
		int32_t *value = (int32_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			int32_t _value = info[2].As<Number>();
			*value = (int32_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "uint32"){
		int shmid = shmget(key, sizeof(uint32_t), 0666|IPC_CREAT);
		uint32_t *value = (uint32_t*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			uint32_t _value = info[2].As<Number>();
			*value = (uint32_t) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "float"){
		int shmid = shmget(key, sizeof(float), 0666|IPC_CREAT);
		float *value = (float*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			float _value = info[2].As<Number>();
			*value = (float) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else if(datatype == "double"){
		int shmid = shmget(key, sizeof(double), 0666|IPC_CREAT);
		double *value = (double*) shmat(shmid, NULL, 0);

		if(info.Length() == 3){
			double _value = info[2].As<Number>();
			*value = (double) _value;
		}

		retVal = Number::New(env, *value);
		shmdt(value);
	} else {
		TypeError::New(env, "Data Type is not Known!")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	return retVal;
}

Object Init(Env env, Object exports) {
	exports.Set(String::New(env, "ftok"), Function::New(env, SHMOP_ftok));
	exports.Set(String::New(env, "shmat"), Function::New(env, SHMOP_shmat));
	return exports;
}

NODE_API_MODULE(shmop, Init);
